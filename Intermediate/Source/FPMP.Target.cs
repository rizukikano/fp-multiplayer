using UnrealBuildTool;

public class FPMPTarget : TargetRules
{
	public FPMPTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("FPMP");
	}
}
